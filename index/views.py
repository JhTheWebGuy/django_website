from django.shortcuts import render



def index(request):
    return render(request, 'index/home.html')

def contact(request):
    return render(request, 'index/basic.html', {'content':['If you would like to contact me, email']})
